//
//  AppHelper.m
//  OurCity
//
//  Created by Kirill Gorbushko on 23.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCAppHelper.h"
#import "OUCAppDelegate.h"

@implementation OUCAppHelper

#pragma mark - Public

+ (NSString *)appName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleNameKey];
}

+ (NSDate *)dateFromString:(NSString *)dateString
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"dd.MM.yyyy hh:mm:ss";
    formatter.timeZone = [NSTimeZone localTimeZone];
    return [formatter dateFromString:dateString];
}

+ (NSNumber *)city
{
    return [(OUCAppDelegate *)[UIApplication sharedApplication].delegate cityId];
}

@end
