//
//  AppHelper.h
//  OurCity
//
//  Created by Kirill Gorbushko on 23.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

@interface OUCAppHelper : NSObject

+ (NSDate *)dateFromString:(NSString *)dateString;

+ (NSNumber *)city;

@end
