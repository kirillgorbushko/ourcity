//
//  Constants.h
//  OurCity
//
//  Created by Kirill Gorbushko on 23.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#pragma mark - Defines

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#pragma mark - Keys

static NSString *const GoogleMapsAPIKey = @"AIzaSyAqBcYNoQvvOFVn4YiWckjbMXyFF1EHsTc";

#pragma mark - XibNames

static NSString *const NibNameOUCMenuViewController = @"OUCMenuViewController";
static NSString *const NibNameOUCHeaderView = @"OUCHeaderView";
static NSString *const NibNameOUCSplashScreenViewController = @"OUCSplashScreenViewController";
static NSString *const NibNameOUCMenuCollectionViewCell = @"OUCMenuCollectionViewCell";
static NSString *const NibNameOUCSpeedAccessCollectionViewCell = @"OUCSpeedAccessCollectionViewCell";
static NSString *const NibNameOUCInformationViewController = @"OUCInformationViewController";
static NSString *const NibNameOUCSettingsTableViewCell = @"OUCSettingsTableViewCell";
static NSString *const NibNameOUCSettingsViewController = @"OUCSettingsViewController";
static NSString *const NibNameOUCRegisterViewController = @"OUCRegisterViewController";
static NSString *const NibNameOUCMapViewController = @"OUCMapViewController";