//
//  AppDelegate.m
//  OurCity
//
//  Created by Kirill Gorbushko on 23.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCAppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "NetworkManager.h"
#import "OUCMenuViewController.h"
#import "StorageManager.h"

#import "OUCMapViewController.h"

@interface OUCAppDelegate ()

@end

@implementation OUCAppDelegate

#pragma mark - LifeCycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [DynamicUIService service];
    
#warning cityId
    
    self.cityId = @1;
    
    [self configureMaps];
    [self registerForPushNotification];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    OUCMenuViewController *menuViewController = [[OUCMenuViewController alloc] initWithNibName:NibNameOUCMenuViewController bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
    [navigationController setNavigationBarHidden:YES];
    
    self.window.rootViewController = navigationController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    [StorageManager sharedManager].deviceToken = token;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // TODO:
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Error: %@", error.localizedDescription);
}

#pragma mark - Private Methods

- (void)configureMaps
{
    [GMSServices provideAPIKey:GoogleMapsAPIKey];
}

- (void)registerForPushNotification
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType types = (UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound)];
    }
}

@end
