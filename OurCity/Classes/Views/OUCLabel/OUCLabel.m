//
//  OUCLabel.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/29/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "OUCLabel.h"

@implementation OUCLabel

- (void)drawTextInRect:(CGRect)rect
{
    UIEdgeInsets insets = {0, 15, 0, 15};
    [super drawTextInRect:(UIEdgeInsetsInsetRect(rect, insets))];
}

@end
