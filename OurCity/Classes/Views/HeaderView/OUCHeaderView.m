//
//  OUCHeaderView.m
//  OurCity
//
//  Created by Kirill Gorbushko on 23.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCHeaderView.h"

@implementation OUCHeaderView

#pragma mark - Custom Accessors

-(void)setHeaderViewMode:(OUCHeaderViewMode)headerViewMode
{
    _headerViewMode = headerViewMode;
    [self setMode];
}

#pragma mark - IBActions

- (IBAction)settingsButtonPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerViewSettingButtonDidPressed:)]) {
        [self.delegate  headerViewSettingButtonDidPressed:sender];
    }
}

- (IBAction)backButtonPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerViewBackButtonDidPressed:)]) {
        [self.delegate  headerViewBackButtonDidPressed:sender];
    }
}

- (IBAction)homeButtonPressed:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerViewHomeButtonDidPressed:)]) {
        [self.delegate  headerViewHomeButtonDidPressed:sender];
    }
}

#pragma mark - Private

- (void)setMode
{
    switch (self.headerViewMode) {
        case OUCHeaderViewModeSettingsOnly: {
            self.headerHomeButton.hidden = YES;
            self.headerBackButton.hidden = YES;
            break;
        }
        case OUCHeaderViewModeSettingBack: {
            self.headerHomeButton.hidden = YES;
            self.headerBackButton.hidden = NO;
            break;
        }
        case OUCHeaderViewModeUndefined: {
            self.headerHomeButton.hidden = NO;
            self.headerBackButton.hidden = NO;
            break;
        }
    }
}

@end
