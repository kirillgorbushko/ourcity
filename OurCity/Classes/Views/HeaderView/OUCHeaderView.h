//
//  OUCHeaderView.h
//  OurCity
//
//  Created by Kirill Gorbushko on 23.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCBaseView.h"

@protocol OUCHeaderViewDelegate <NSObject>

@required
- (void)headerViewSettingButtonDidPressed:(id)sender;

@optional
- (void)headerViewBackButtonDidPressed:(id)sender;
- (void)headerViewHomeButtonDidPressed:(id)sender;

@end

typedef NS_ENUM(NSUInteger, OUCHeaderViewMode) {
    OUCHeaderViewModeUndefined = 0,
    OUCHeaderViewModeAll = 0,
    OUCHeaderViewModeSettingsOnly = 1,
    OUCHeaderViewModeSettingBack = 2,
};

@interface OUCHeaderView : OUCBaseView

@property (weak, nonatomic) IBOutlet UIView *headerTopView;
@property (weak, nonatomic) IBOutlet UIView *headerBottomView;
@property (weak, nonatomic) IBOutlet UIButton *heasderSettingButton;
@property (weak, nonatomic) IBOutlet UIButton *headerBackButton;
@property (weak, nonatomic) IBOutlet UIButton *headerHomeButton;
@property (weak, nonatomic) IBOutlet UIImageView *headerBackgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (weak, nonatomic) id <OUCHeaderViewDelegate> delegate;

@property (assign, nonatomic) OUCHeaderViewMode headerViewMode;

@end
