//
//  OUCMenuCollectionViewCell.m
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCMenuCollectionViewCell.h"

@interface OUCMenuCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIView *menuCellBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *menuCellLogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuCellTitleLabel;

@end

@implementation OUCMenuCollectionViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.menuCellBackgroundView.layer.cornerRadius = 3.f;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.menuCellBackgroundView.backgroundColor = [UIColor whiteColor];
    self.menuCellLogoImageView.image = nil;
    self.menuCellTitleLabel.text = @"";
}

#pragma mark - Custom Accessors

- (void)setMenuItem:(MenuItem *)menuItem
{
    _menuItem = menuItem;
    [self configureCell];
}

#pragma mark - Private Methods

- (void)configureCell
{
    self.menuCellTitleLabel.text = self.menuItem.menuItemTitle;
    self.menuCellBackgroundView.backgroundColor = self.menuItem.menuItemColor;
    // TODO: item icon
    
    [[NetworkManager sharedManager] downloadImageWithContentsOfURL:self.menuItem.menuItemPicturePath result:^(BOOL success, id response, NSError *error) {
        if (success) {
           self.menuCellLogoImageView.image = response;
        } else {
            // TODO: placeholder 
        }
    }];
}

@end
