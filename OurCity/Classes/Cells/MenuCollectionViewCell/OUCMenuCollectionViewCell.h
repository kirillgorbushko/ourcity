//
//  OUCMenuCollectionViewCell.h
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "MenuItem.h"

static NSString *const OUCMenuCellIdentifier = @"menuCell";

@interface OUCMenuCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) MenuItem *menuItem;

@end
