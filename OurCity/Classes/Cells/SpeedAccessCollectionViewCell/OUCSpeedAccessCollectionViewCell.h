//
//  OUCSpeedAccessCollectionViewCell.h
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

static NSString *const OUCSpeedAccessCollectionViewCellIdentifier = @"speedAccessCell";

typedef NS_ENUM(NSUInteger, SpeedAccessCellType) {
    SpeedAccessCellTypeUndefined = 0,
    SpeedAccessCellTypeLeftImage = 0,
    SpeedAccessCellTypeRightImage = 1,
    SpeedAccessCellTypeBothImage = 2,
    SpeedAccessCellTypeNoImage = 3
};

@interface OUCSpeedAccessCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *leftIconImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rightIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)setCellType:(SpeedAccessCellType)type;
@end
