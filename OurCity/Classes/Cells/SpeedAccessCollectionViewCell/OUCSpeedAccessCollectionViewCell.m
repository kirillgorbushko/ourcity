//
//  OUCSpeedAccessCollectionViewCell.m
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCSpeedAccessCollectionViewCell.h"

static CGFloat MinimumLabelOffset = 8.f;

@interface OUCSpeedAccessCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *leftImageViewContainer;
@property (weak, nonatomic) IBOutlet UIView *rightImageViewContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingTitleLabelConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tralingTitleLabelConstraint;

@property (assign, nonatomic) SpeedAccessCellType speedAccessCellType;

@end

@implementation OUCSpeedAccessCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.bottomView.layer.cornerRadius = 3.f;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self setCellType:self.speedAccessCellType];
    
    self.leftIconImageView.image = nil;
    self.rightIconImageView.image = nil;
    self.titleLabel.text = @"";
}

#pragma mark - Public

- (void)setCellType:(SpeedAccessCellType)type
{
    self.speedAccessCellType = type;
    switch (type) {
        case SpeedAccessCellTypeLeftImage: {
            self.rightImageViewContainer.hidden = YES;
            self.tralingTitleLabelConstraint.constant = MinimumLabelOffset;
            break;
        }
        case SpeedAccessCellTypeRightImage: {
            self.leftImageViewContainer.hidden = YES;
            self.leadingTitleLabelConstraint.constant = MinimumLabelOffset;
            break;
        }
        case SpeedAccessCellTypeBothImage: {
            break;
        }
        case SpeedAccessCellTypeNoImage: {
            self.leftImageViewContainer.hidden = YES;
            self.leadingTitleLabelConstraint.constant = MinimumLabelOffset;
            self.rightImageViewContainer.hidden = YES;
            self.tralingTitleLabelConstraint.constant = MinimumLabelOffset;
            break;
        }
    }
    [self setNeedsUpdateConstraints];
    [self layoutIfNeeded];
}

@end
