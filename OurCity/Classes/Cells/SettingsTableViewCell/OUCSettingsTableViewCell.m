//
//  OUCSettingsTableViewCell.m
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCSettingsTableViewCell.h"

@interface OUCSettingsTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *settingContentView;

@end

@implementation OUCSettingsTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.settingContentView.layer.cornerRadius = 3.f;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.logoImageView.image = nil;
    self.itemNameLabel.text = @"";
}

@end
