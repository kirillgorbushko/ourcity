//
//  OUCSettingsTableViewCell.h
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

static NSString *const OUCSettingsTableViewCellIdentifier = @"settingsCell";

@interface OUCSettingsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;

@end
