//
//  OUCBasePresentedViewController.h
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

@interface OUCBasePresentedViewController : UIViewController

- (IBAction)closeButtonPress:(id)sender;

@end
