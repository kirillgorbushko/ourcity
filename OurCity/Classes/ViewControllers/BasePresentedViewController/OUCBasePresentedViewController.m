//
//  OUCBasePresentedViewController.m
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCBasePresentedViewController.h"
#import "Animation.h"

@interface OUCBasePresentedViewController ()

@end

@implementation OUCBasePresentedViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1 delegate:nil] forKey:nil];
}

#pragma mark - IBActions

- (IBAction)closeButtonPress:(id)sender
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1 to:0 delegate:self] forKey:@"hide"];
    self.view.layer.opacity = 0.0f;
}

#pragma mark - Animation

- (void)animationDidStop:(nonnull CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"hide"]) {
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }
}

@end
