//
//  OUCBaseViewController.h
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

@interface OUCBaseViewController : UIViewController <OUCHeaderViewDelegate>

@property (weak, nonatomic) IBOutlet OUCHeaderView *headerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceConstraint;

@end
