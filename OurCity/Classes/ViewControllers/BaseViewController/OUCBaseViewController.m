//
//  OUCBaseViewController.m
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCBaseViewController.h"
#import "OUCSettingsViewController.h"

@interface OUCBaseViewController ()

@end

@implementation OUCBaseViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareTopBar];
    [self pinTopSpaceConstraintToTopLayoutGuide];
}

#pragma mark - OUCHeaderViewDelegate

- (void)headerViewSettingButtonDidPressed:(id)sender
{
    OUCSettingsViewController *settingViewController = [[OUCSettingsViewController alloc] initWithNibName:NibNameOUCSettingsViewController bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:settingViewController];
    [navigationController setNavigationBarHidden:YES];
    navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
    navigationController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
#endif
    [self.navigationController presentViewController:navigationController animated:NO completion:nil];
}

 - (void)headerViewBackButtonDidPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)headerViewHomeButtonDidPressed:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Private

- (void)prepareTopBar
{
    self.headerView.delegate = self;
    self.headerView.headerViewMode = OUCHeaderViewModeSettingsOnly;
}

- (void)pinTopSpaceConstraintToTopLayoutGuide
{
    if ([self respondsToSelector:@selector(topLayoutGuide)]) {
        [self.view removeConstraint:self.topSpaceConstraint];
        self.topSpaceConstraint = [NSLayoutConstraint constraintWithItem:self.headerView
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.topLayoutGuide
                                                               attribute:NSLayoutAttributeBottom
                                                              multiplier:1
                                                                constant:0];
        [self.view addConstraint:self.topSpaceConstraint];
        [self.view setNeedsUpdateConstraints];
        [self.view layoutIfNeeded];
    }
}

@end