//
//  OUCWebViewController.m
//  OurCity
//
//  Created by Anatoliy Dalekorey on 7/24/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "OUCWebViewController.h"

@interface OUCWebViewController ()

@property (strong, nonatomic) NSString *url;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation OUCWebViewController

#pragma mark - Life Cycle

- (instancetype)initWithURL:(NSString *)url
{
    self = [super init];
    if (self) {
        _url = [url copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadRequestFromString:self.url];
    self.headerView.headerViewMode = OUCHeaderViewModeAll;
}

#pragma mark - UIWebView Delegate

- (void)loadRequestFromString:(NSString*)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:urlRequest];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
