//
//  OUCWebViewController.h
//  OurCity
//
//  Created by Anatoliy Dalekorey on 7/24/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "OUCBaseViewController.h"

@interface OUCWebViewController : OUCBaseViewController <UIWebViewDelegate>

- (instancetype)initWithURL:(NSString *)url;

@end
