//
//  OUCRegisterViewController.m
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCRegisterViewController.h"

@interface OUCRegisterViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *contentBackgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobilePhoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *houseNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *streetNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastTextField;
@property (weak, nonatomic) IBOutlet UILabel *informationBottomLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UIView *registerTextFieldContainerView;

@end

@implementation OUCRegisterViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self performLocalization];
    [self registerKeyboardNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self removeKeyboardNotification];
}

#pragma mark - IBActions

- (IBAction)checkedButtonPressed:(UIButton *)sender
{
    if (sender.selected) {
        [sender setImage:nil forState:UIControlStateNormal];
    } else {
        [sender setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    }
    sender.selected = !sender.selected;
}

- (IBAction)registerButtonPressed:(id)sender
{
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextResponderNumber = textField.tag + 1;
    UIResponder *nextresponder = [textField.superview viewWithTag:nextResponderNumber];
    if (nextresponder) {
        [nextresponder becomeFirstResponder];
        return NO;
    } else {
        [textField resignFirstResponder];
        [self.view endEditing:YES];
    }
    return YES;
}

#pragma mark - Private

#pragma mark - Notification

- (void)registerKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillShowNotification:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardRect.size.height;
    
    CGFloat bottomRequiredVisiblePointYPosition = self.registerButton.frame.origin.y + self.registerButton.frame.size.height;
    CGFloat offsetForScrollViewY = keyboardHeight - ([UIScreen mainScreen].bounds.size.height - bottomRequiredVisiblePointYPosition);
    
    if (IS_IPHONE_4_OR_LESS) {
        offsetForScrollViewY += 50.f;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        [weakSelf.scrollView setContentOffset:CGPointMake(0, offsetForScrollViewY < 50.f ? 50.f : offsetForScrollViewY)];
        [weakSelf.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHideNotification:(NSNotification *)notification
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        [weakSelf.scrollView setContentOffset:CGPointZero];
        [weakSelf.view layoutIfNeeded];
    }];
}

#pragma mark - UI

- (void)prepareUI
{
    for (UIView *view in self.registerTextFieldContainerView.subviews) {
        if ([view isKindOfClass:[UITextField class]] && view.tag >= 0) {
            view.layer.borderColor = [UIColor greenBorderColor].CGColor;
            view.layer.borderWidth = 2.f;
        }
        view.layer.cornerRadius = 5.f;
        view.layer.masksToBounds = YES;
    }
}

- (void)performLocalization
{
    self.firstNameTextField.placeholder = dynamicLocalizedString(@"register.textField.placeholder.firstName");
    self.lastNameTextField.placeholder = dynamicLocalizedString(@"register.textField.placeholder.secondName");
    self.mobilePhoneNumberTextField.placeholder = dynamicLocalizedString(@"register.textField.placeholder.mobile");
    self.phoneNumberTextField.placeholder = dynamicLocalizedString(@"register.textField.placeholder.phone");
    self.emailTextField.placeholder = dynamicLocalizedString(@"register.textField.placeholder.email");
    self.houseNumberTextField.placeholder = dynamicLocalizedString(@"register.textField.placeholder.homeNumber");
    self.streetNumberTextField.placeholder = dynamicLocalizedString(@"register.textField.placeholder.streetAddress");
    self.lastTextField.placeholder = dynamicLocalizedString(@"register.textField.placeholder.last");
    self.informationBottomLabel.text = dynamicLocalizedString(@"register.label.info");
    [self.registerButton setTitle: dynamicLocalizedString(@"register.button.register") forState:UIControlStateNormal];
}

@end
