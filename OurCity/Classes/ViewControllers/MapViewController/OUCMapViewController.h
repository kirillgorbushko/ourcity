//
//  OUCMapViewController.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/29/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OUCBaseViewController.h"

@interface OUCMapViewController : UIViewController

@property (strong, nonatomic) NSArray *markers;

@end
