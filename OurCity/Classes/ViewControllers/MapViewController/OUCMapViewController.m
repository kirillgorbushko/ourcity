//
//  OUCMapViewController.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/29/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "OUCMapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "GoogleMapsManager.h"
#import "OUCLabel.h"

@interface OUCMapViewController ()

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet OUCLabel *titleLabel;

@end

@implementation OUCMapViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareMapView];
}

#pragma mark - Private Methods

- (void)prepareMapView
{
    [GoogleMapsManager setMarkers:self.markers onMapView:self.mapView];
}

@end
