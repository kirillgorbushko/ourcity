//
//  OUCSettingsViewController.m
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCSettingsViewController.h"
#import "OUCSettingsTableViewCell.h"
#import "OUCRegisterViewController.h"

@interface OUCSettingsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleSettingsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *settingsIconImageView;

@property (strong, nonatomic) NSArray *dataSource;

@end

@implementation OUCSettingsViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareTableView];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;//self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OUCSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:OUCSettingsTableViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[OUCSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:OUCSettingsTableViewCellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!indexPath.row) {
        OUCRegisterViewController *registerViewController = [[OUCRegisterViewController alloc] initWithNibName:NibNameOUCRegisterViewController bundle:nil];
        registerViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
        registerViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
#endif
        [self.navigationController presentViewController:registerViewController animated:NO completion:nil];
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56.f;
}

#pragma mark - CellConfiguration

- (void)configureCell:(OUCSettingsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

#pragma mark - Private

- (void)prepareTableView
{
    [self.tableView registerNib:[UINib nibWithNibName:NibNameOUCSettingsTableViewCell bundle:nil] forCellReuseIdentifier:OUCSettingsTableViewCellIdentifier];
#warning UPDATE_NAME_HERE :]
    UIImage *image = [UIImage imageNamed:@"1"];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:image];
}

@end