//
//  OUCSettingsViewController.h
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//
#import "OUCBasePresentedViewController.h"


@interface OUCSettingsViewController : OUCBasePresentedViewController

@end
