//
//  OUCSplashScreenViewController.m
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCSplashScreenViewController.h"
#import "Animation.h"

@interface OUCSplashScreenViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *splashImageView;

@end

@implementation OUCSplashScreenViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareUI];
}

#pragma mark - Public

- (void)hideViewController
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1 to:0 delegate:self] forKey:@"hide"];
    self.view.layer.opacity = 0.0f;
}

#pragma mark - Animation

- (void)animationDidStop:(nonnull CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"hide"]) {
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }
}

#pragma mark - Private

- (void)prepareUI
{
    if (self.splashImage) {
        self.splashImageView.image = self.splashImage;
    }
}

@end