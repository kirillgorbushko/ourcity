//
//  OUCSplashScreenViewController.h
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

@interface OUCSplashScreenViewController : UIViewController

@property (strong, nonatomic) UIImage *splashImage;

- (void)hideViewController;

@end
