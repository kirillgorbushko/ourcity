//
//  OUCInformationViewController.m
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCInformationViewController.h"

@interface OUCInformationViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *topSeparatorView;
@property (weak, nonatomic) IBOutlet UIView *bottomSeparatorView;
@property (weak, nonatomic) IBOutlet UITextView *informationTextView;
@property (weak, nonatomic) IBOutlet UIImageView *portraintImageView;

@end

@implementation OUCInformationViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setImageIfNeeded];
    self.headerView.headerViewMode = OUCHeaderViewModeAll;
}

#pragma mark - Private

- (void)setImageIfNeeded
{
    if (self.portraintImage) {
        
        self.portraintImageView.image = self.portraintImage;
        
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        CGFloat offset = 5.f;
        
        CGFloat xPoint = screenSize.width - self.portraintImageView.frame.size.width - offset - self.informationTextView.frame.origin.x * 2;
        CGFloat height = self.portraintImageView.frame.size.height;
        CGFloat width = self.portraintImageView.frame.size.width + offset;
        
        CGRect exclusionRect = CGRectMake(xPoint, 0, width, height);
        
        UIBezierPath *imagePath = [UIBezierPath bezierPathWithRect:exclusionRect];
        self.informationTextView.textContainer.exclusionPaths = @[imagePath];
    }
}

@end