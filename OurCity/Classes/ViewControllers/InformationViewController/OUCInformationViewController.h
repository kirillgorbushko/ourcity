//
//  OUCInformationViewController.h
//  OurCity
//
//  Created by Kirill Gorbushko on 24.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCBaseViewController.h"

@interface OUCInformationViewController : OUCBaseViewController

@property (strong, nonatomic) UIImage *portraintImage;
@property (copy, nonatomic) NSString *InformationText;

@end
