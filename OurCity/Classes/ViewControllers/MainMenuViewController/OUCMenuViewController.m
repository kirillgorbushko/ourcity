//
//  MenuViewController.m
//  OurCity
//
//  Created by Kirill Gorbushko on 23.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//

#import "OUCMenuViewController.h"
#import "OUCSplashScreenViewController.h"
#import "OUCMenuCollectionViewCell.h"
#import "OUCSpeedAccessCollectionViewCell.h"
#import "OUCInformationViewController.h"
#import "OUCWebViewController.h"
#import "MenuItem.h"

static CGFloat const MenuRowsCount = 1;
static CGFloat const MinimumCellSpacing = 3;

@interface OUCMenuViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *menuCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *speedAccessCollectionView;

@property (strong, nonatomic) OUCSplashScreenViewController *splashViewController;

@property (strong, nonatomic) NSArray *menuDataSource;
@property (strong, nonatomic) NSArray *speedAccessDataSource;

@end

@implementation OUCMenuViewController
#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNibs];
    [self prepareDataSources];
    [self presentSplashScreenWhileLoadingData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.menuCollectionView reloadData];
    [self.speedAccessCollectionView reloadData];
}

#pragma mark - IBActions

- (void)hideSplash
{
    [self.splashViewController hideViewController];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.menuCollectionView) {
        MenuItem *menuItem = self.menuDataSource[indexPath.row];
        
        
//        [self.navigationController pushViewController:self.menuDataSource[1] animated:YES];
    } else if (collectionView == self.speedAccessCollectionView) {
        
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger itemsCount = self.speedAccessDataSource.count;
    if (collectionView == self.menuCollectionView) {
        itemsCount = self.menuDataSource.count;
    }
    return itemsCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if (collectionView == self.menuCollectionView) {
        OUCMenuCollectionViewCell *menuCell = [collectionView dequeueReusableCellWithReuseIdentifier:OUCMenuCellIdentifier forIndexPath:indexPath];
        if (!menuCell) {
            menuCell = [[OUCMenuCollectionViewCell alloc] init];
        }
        [self configureMenuCell:menuCell atIndexPath:indexPath];
        cell = menuCell;
    } else if (collectionView == self.speedAccessCollectionView) {
        OUCSpeedAccessCollectionViewCell *speedAccessCell = [collectionView dequeueReusableCellWithReuseIdentifier:OUCSpeedAccessCollectionViewCellIdentifier forIndexPath:indexPath];
        if (!speedAccessCell) {
            speedAccessCell = [[OUCSpeedAccessCollectionViewCell alloc] init];
        }
        [self configureSpeedAccessCell:speedAccessCell atIndexPath:indexPath];
        cell = speedAccessCell;
    }
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize;
    CGSize contentSize;
    NSUInteger numberOfRows;
    NSUInteger numberOfColums;
    
    if (collectionView == self.menuCollectionView) {
        contentSize = self.menuCollectionView.frame.size;
        numberOfColums = self.menuDataSource.count / MenuRowsCount;
        CGFloat sideSize = (contentSize.width - (MinimumCellSpacing * (numberOfColums))) / numberOfColums;
        cellSize = CGSizeMake(sideSize, sideSize);
    } else if (collectionView == self.speedAccessCollectionView) {
        contentSize = self.speedAccessCollectionView.frame.size;
        numberOfRows = 1;
        numberOfColums = self.speedAccessDataSource.count / numberOfRows;
        cellSize = CGSizeMake((contentSize.width - (MinimumCellSpacing * (numberOfColums + 1))) / numberOfColums,
                              (contentSize.height - (MinimumCellSpacing * (numberOfRows + 1))) / numberOfRows);
    }
    
    return cellSize;
}

#pragma mark - CellConfiguration

- (void)configureMenuCell:(OUCMenuCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.menuItem = self.menuDataSource[indexPath.row];
}

- (void)configureSpeedAccessCell:(OUCSpeedAccessCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.row) {
        [cell setCellType:SpeedAccessCellTypeRightImage];
    } else {
        [cell setCellType:SpeedAccessCellTypeLeftImage];
    }
    cell.titleLabel.text = @"Speed 1";
    cell.bottomView.backgroundColor = [UIColor redColor];
}

#pragma mark - Private

- (void)presentSplashScreenWhileLoadingData
{
    self.splashViewController = [[OUCSplashScreenViewController alloc] initWithNibName:NibNameOUCSplashScreenViewController bundle:nil];
    [self.view addSubview:self.splashViewController.view];
    [self addChildViewController:self.splashViewController];
    [self.splashViewController didMoveToParentViewController:self];
    
#warning test
    //timer for test
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hideSplash) userInfo:nil repeats:NO];
}

- (void)registerNibs
{
    [self.menuCollectionView registerNib:[UINib nibWithNibName:NibNameOUCMenuCollectionViewCell bundle:nil] forCellWithReuseIdentifier:OUCMenuCellIdentifier];
    [self.speedAccessCollectionView registerNib:[UINib nibWithNibName:NibNameOUCSpeedAccessCollectionViewCell bundle:nil] forCellWithReuseIdentifier:OUCSpeedAccessCollectionViewCellIdentifier];
}

- (void)prepareDataSources
{
    [[NetworkManager sharedManager] ourCityAPIGetMobileMenuForCityWithLanguage:@"he" completionHandler:^(BOOL success, id response, NSError *error) {
        if (success) {
            self.menuDataSource = response;
            [self.menuCollectionView reloadData];
        }
    }];
    
    self.speedAccessDataSource = @[@"", @""];
}

@end