//
//  MenuViewController.h
//  OurCity
//
//  Created by Kirill Gorbushko on 23.07.15.
//  Copyright © 2015 Thinkmobiles. All rights reserved.
//
#import "OUCBaseViewController.h"

@interface OUCMenuViewController : OUCBaseViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@end
