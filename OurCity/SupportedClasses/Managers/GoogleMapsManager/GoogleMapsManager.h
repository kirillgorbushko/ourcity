//
//  GoogleMapsManager.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/29/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface GoogleMapsManager : NSObject

+ (void)setMarkers:(NSArray *)markers onMapView:(GMSMapView *)mapView;

@end
