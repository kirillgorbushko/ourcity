//
//  StorageManager.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/28/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StorageManager : NSObject

@property (strong, nonatomic) NSString *deviceToken;

+ (instancetype)sharedManager;

@end
