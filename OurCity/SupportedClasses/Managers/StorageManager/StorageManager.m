//
//  StorageManager.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/28/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "StorageManager.h"

@implementation StorageManager

+ (instancetype)sharedManager
{
    static StorageManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [StorageManager new];
    });
    return manager;
}

@end
