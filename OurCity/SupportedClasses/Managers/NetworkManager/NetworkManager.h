//
//  NetworkManager.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/28/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CompletionHandler)(BOOL success, id response, NSError *error);

@interface NetworkManager : NSObject

+ (instancetype)sharedManager;

#pragma mark - Login / Register

- (void)ourCityAPIRegisterUserWithName:(NSString *)name surname:(NSString *)surname email:(NSString *)email mobileNumber:(NSString *)mobileNumber phoneNumber:(NSString *)phoneNumber houseNumber:(NSInteger)houseNumber enableGlobalNotification:(BOOL)globalNotification enablePersonalNotification:(BOOL)personalNotification streetID:(NSInteger)streetID cityID:(NSInteger)cityID registerResult:(CompletionHandler)registerResult;

#pragma mark - Menu

- (void)ourCityAPIGetMobileMenuForCityWithLanguage:(NSString *)language completionHandler:(CompletionHandler)completionHandler;

#pragma mark - City

- (void)ourCityAPIGetCategoriesToCityWithCompletion:(CompletionHandler)completionHandler;
- (void)ourCityAPIGetEventsToCityWithTypeName:(NSString *)typeName completion:(CompletionHandler)completionHandler;
- (void)ourCityAPIGetCoursesToCityWithCompletion:(CompletionHandler)completionHandler;
- (void)ourCityAPIGetEntitiesToCityWithCompletion:(CompletionHandler)completionHandler;
- (void)ourCityAPIGetEntityCategoriesToCityWithCompletion:(CompletionHandler)completionHandler;
- (void)ourCityAPIGetCityUpdatingsToCityWithCompletion:(CompletionHandler)completionHandler;
- (void)ourCityAPIGetPhonebookCategoryToCityWithCompletion:(CompletionHandler)completionHandler;
- (void)ourCityAPIGetInterestAreaToCityWithCompletion:(CompletionHandler)completionHandler;
- (void)ourCityAPIGetCityContentsWithCompletion:(CompletionHandler)completionHandler;

#pragma mark - Map

- (void)ourCityAPIGetTripsWithResult:(CompletionHandler)result;
- (void)ourCityAPIGetInterestPointToCityWithID:(NSInteger)cityID result:(CompletionHandler)result;

#pragma mark - Resident 

- (void)ourCityAPIGetMessagesForResidentWithID:(NSInteger)residentID result:(CompletionHandler)result;

#pragma mark - Images

- (void)downloadImageWithContentsOfURL:(NSString *)imagePath result:(CompletionHandler)downloadResult;

- (void)ourCityAPIGetLogoImageForCityWithID:(NSInteger)cityID result:(CompletionHandler)result;
- (void)ourCityAPIGetCityImageForCityWithID:(NSInteger)cityID result:(CompletionHandler)result;
- (void)ourCityAPIGetMayorImageForCityWithID:(NSInteger)cityID result:(CompletionHandler)result;

#pragma mark -

- (void)performPOST:(NSString *)requestRoute requestJSON:(NSString *)requestJSON completionHandler:(CompletionHandler)completionHandler;

#pragma mark - Rechability

- (void)configureReachabilityManager;

@end
