//
//  NetworkManager.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/28/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "NetworkManager.h"
#import "NetworkManagerConstants.h"
#import "AFNetworking.h"
#import "UIImage+Base64.h"
#import "MenuItem.h"
#import "CityCategory.h"
#import "Event.h"
#import "Course.h"
#import "Entity.h"
#import "Message.h"
#import "Update.h"
#import "PhonebookCategory.h"
#import "InterestArea.h"
#import "CityContent.h"
#import "Trip.h"

@interface NetworkManager()

@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;

@end

@implementation NetworkManager

+ (instancetype)sharedManager
{
    static NetworkManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [NetworkManager new];
    });
    
    return manager;
}

#pragma mark - Lifecycle 

- (instancetype)init
{
    if (self = [super init]) {
        [self initializeManager];
        [self configureReachabilityManager];
    }
    return self;
}

- (void)initializeManager
{
    NSURL *baseURL = [NSURL URLWithString:baseURLString];
    self.manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    self.manager.securityPolicy = securityPolicy;
}

#pragma mark - Login / Register

- (void)ourCityAPIRegisterUserWithName:(NSString *)name surname:(NSString *)surname email:(NSString *)email mobileNumber:(NSString *)mobileNumber phoneNumber:(NSString *)phoneNumber houseNumber:(NSInteger)houseNumber enableGlobalNotification:(BOOL)globalNotification enablePersonalNotification:(BOOL)personalNotification streetID:(NSInteger)streetID cityID:(NSInteger)cityID registerResult:(CompletionHandler)registerResult
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:
                                 @{
                                     @"resident":@{
                                                     @"firstName":name,
                                                     @"lastName":surname,
                                                     @"email":email,
                                                     @"mobileNumber":mobileNumber,
                                                     @"phoneNumber":phoneNumber,
                                                     @"houseNumber":@(houseNumber),
                                                     @"applianceType":@"IOS",
                                                     @"isGetGlobalNotification":globalNotification ? @"true" : @"false",
                                                     @"isGetPersonalNotification":personalNotification ? @"true" : @"false",
                                                     @"iStreetId":@(streetID),
                                                     @"cityId":@(cityID)
                                                  }
                                   }];

    [self.manager POST:ourCityRegistNewResident parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *userId = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        if (registerResult) {
            registerResult(YES, userId, nil);
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (registerResult) {
            registerResult(NO, operation, error);
        }
    }];
}

#pragma mark - Menu

- (void)ourCityAPIGetMobileMenuForCityWithLanguage:(NSString *)language completionHandler:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city], @"lng":language}};
    
    [self.manager POST:ourCityMobileMenu parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (completionHandler) {
                completionHandler(NO, nil, error);
            }
        } else {
            if (completionHandler) {
                completionHandler(YES, [MenuItem getListOfMenuItemsFromDictionary:json[@"Nodes"]], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

#pragma mark - City

- (void)ourCityAPIGetCategoriesToCityWithCompletion:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city]}};
    
    [self.manager POST:ourCityCategories parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (completionHandler) {
                completionHandler(NO, nil, error);
            }
        } else {
            if (completionHandler) {
                completionHandler(YES, [CityCategory getAllCityCategoriesFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

- (void)ourCityAPIGetEventsToCityWithTypeName:(NSString *)typeName completion:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"type":@{@"typeName":typeName, @"cityId":[OUCAppHelper city]}};
   
    [self.manager POST:ourCityEvents parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (completionHandler) {
                completionHandler(NO, nil, error);
            }
        } else {
            if (completionHandler) {
                completionHandler(YES, [Event getAllEventsFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

- (void)ourCityAPIGetCoursesToCityWithCompletion:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city]}};
    
    [self.manager POST:ourCityCourses parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (completionHandler) {
                completionHandler(NO, nil, error);
            }
        } else {
            if (completionHandler) {
                completionHandler(YES, [Course getAllCoursesFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(YES, operation, error);
        }
    }];
}

- (void)ourCityAPIGetEntitiesToCityWithCompletion:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city]}};
    
    [self.manager POST:ourCityEntities parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        if (error) {
            if (completionHandler) {
                completionHandler(NO, nil, error);
            }
        } else {
            if (completionHandler) {
                completionHandler(YES, [Entity getAllEntitiesFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

- (void)ourCityAPIGetEntityCategoriesToCityWithCompletion:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city]}};
    
    [self.manager POST:ourCityEntityCategory parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (completionHandler) {
                completionHandler(NO, nil, error);
            }
        } else {
            if (completionHandler) {
                //TODO: entity OR entityCategory ?
                completionHandler(YES, json, nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

- (void)ourCityAPIGetCityUpdatingsToCityWithCompletion:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city]}};
    
    [self.manager POST:ourCityUpdatings parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (completionHandler) {
                completionHandler(NO, nil, error);
            }
        } else {
            if (completionHandler) {
                completionHandler(YES, [Update getAllUpdatingsFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

- (void)ourCityAPIGetPhonebookCategoryToCityWithCompletion:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city]}};
    
    [self.manager POST:ourCityPhonebookCategory parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (completionHandler) {
                completionHandler(NO, nil, error);
            }
        } else {
            if (completionHandler) {
                completionHandler(YES, [PhonebookCategory getAllPhonebookCategoriesFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

- (void)ourCityAPIGetInterestAreaToCityWithCompletion:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city]}};
    [self.manager POST:ourCityInterestArea parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (completionHandler) {
                completionHandler(NO, nil, error);
            }
        } else {
            if (completionHandler) {
                completionHandler(YES, [InterestArea getAllInterestAreasFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

- (void)ourCityAPIGetCityContentsWithCompletion:(CompletionHandler)completionHandler
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city]}};
    [self.manager POST:ourCityContents parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (completionHandler) {
            completionHandler(YES, [CityContent getCityContentFromDictionary:json], nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

#pragma mark - Map

- (void)ourCityAPIGetTripsWithResult:(CompletionHandler)result
{
    NSDictionary *parameters = @{@"city":@{@"cityId":[OUCAppHelper city]}};
    [self.manager POST:ourCityTrips parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (result) {
                result(NO, nil, error);
            }
        } else {
            if (result) {
                result(YES, [Trip getAllTripsFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (result) {
            result(NO, operation, error);
        }
    }];
}

- (void)ourCityAPIGetInterestPointToCityWithID:(NSInteger)cityID result:(CompletionHandler)result
{
    NSDictionary *parameters = @{@"city":@{@"cityId":@(cityID)}};
    
    [self.manager POST:ourCityInterestPoints parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (result) {
                result(NO, nil, error);
            }
        } else {
            if (result) {
                result(YES, [CityCategory getAllCityCategoriesFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (result) {
            result(NO, operation, error);
        }
    }];
}

#pragma mark - Resident

- (void)ourCityAPIGetMessagesForResidentWithID:(NSInteger)residentID result:(CompletionHandler)result
{
    NSDictionary *parameters = @{@"resident":@{@"residentId":@(residentID)}};
    [self.manager POST:ourCityResidentMessages parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error) {
            if (result) {
                result(NO, nil, error);
            }
        } else {
            if (result) {
                result(YES, [Message getAllMessagesFromArray:json], nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (result) {
            result(NO, operation, error);
        }
    }];
}

#pragma mark - Images

- (void)downloadImageWithContentsOfURL:(NSString *)imagePath result:(CompletionHandler)downloadResult
{
    [self.manager GET:imagePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        UIImage *logo = [UIImage imageWithData:responseObject];
        
        if (downloadResult) {
            downloadResult(YES, logo, nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (downloadResult) {
            downloadResult(NO, operation, error);
        }
    }];
}

- (void)ourCityAPIGetLogoImageForCityWithID:(NSInteger)cityID result:(CompletionHandler)result
{
    [self imageWithContentsOfURL:ourCityLogoImage cityID:cityID result:result];
}

- (void)ourCityAPIGetCityImageForCityWithID:(NSInteger)cityID result:(CompletionHandler)result
{
    [self imageWithContentsOfURL:ourCityCityImage cityID:cityID result:result];
}

- (void)ourCityAPIGetMayorImageForCityWithID:(NSInteger)cityID result:(CompletionHandler)result
{
    [self imageWithContentsOfURL:ourCityMayorImage cityID:cityID result:result];
}

- (void)imageWithContentsOfURL:(NSString *)path cityID:(NSInteger)cityID result:(CompletionHandler)result
{
    NSDictionary *parameters = @{@"city":@{@"cityId":@(cityID)}};
    [self.manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *base64Image = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        UIImage *downloadedImage = [UIImage imageFromBase64String:base64Image];
        
        if (downloadedImage) {
            if (result) {
                result(YES, downloadedImage, nil);
            }
        } else {
            if (result) {
                NSError *error = [NSError errorWithDomain:@"123" code:123 userInfo:@{NSLocalizedDescriptionKey:@"Can't download image"}];
                result(NO , nil, error);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (result) {
            result(NO, operation, error);
        }
    }];
}

#pragma mark -

- (void)performPOST:(NSString *)requestRoute requestJSON:(NSString *)requestJSON completionHandler:(CompletionHandler)completionHandler
{
    NSData *parametersData = [requestJSON dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSDictionary *parameters = [NSJSONSerialization JSONObjectWithData:parametersData options:kNilOptions error:&error];
    
    if (error && completionHandler) {
        completionHandler(NO, nil, error);
    }
    
    [self.manager POST:requestRoute parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        if (error && completionHandler) {
            completionHandler(NO, nil, error);
        } else if (!error && completionHandler) {
            completionHandler(YES, json, nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completionHandler) {
            completionHandler(NO, operation, error);
        }
    }];
}

#pragma mark - Rechability

- (void)configureReachabilityManager
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
    }];
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

@end
