//
//  NetworkManagerConstants.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/28/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

static NSString *const baseURLString = @"http://134.249.164.30:8081/OurCityWS/Service.svc";

#pragma mark - Login / Register

static NSString *const ourCityRegistNewResident = @"RegistNewResident";

#pragma mark - Menu

static NSString *const ourCityMobileMenu = @"GetMobileMenu";

#pragma mark - Map

static NSString *const ourCityTrips = @"GetTripsByCityId";
static NSString *const ourCityInterestPoints = @"GetInterestPointToCity";

#pragma mark - City

static NSString *const ourCityCategories = @"GetCategoriesToCity";
static NSString *const ourCityEvents = @"GetEventsByCityIdAndTypeName";
static NSString *const ourCityCourses = @"GetCoursesByCityId";
static NSString *const ourCityEntities = @"GetCityEntities";
static NSString *const ourCityEntityCategory = @"GetCityEntityCategory";
static NSString *const ourCityUpdatings = @"GetCityUpdatings";
static NSString *const ourCityPhonebookCategory = @"GetCityPhonebookCategory";
static NSString *const ourCityInterestArea = @"GetInterestAreaToCity";
static NSString *const ourCityContents = @"GetCityContents";

#pragma mark - Resdent 

static NSString *const ourCityResidentMessages = @"GetMessagesToResident";

#pragma mark - Images

static NSString *const ourCityLogoImage = @"GetLogoImage";
static NSString *const ourCityCityImage = @"GetCityImage";
static NSString *const ourCityMayorImage = @"GetMayorImage";