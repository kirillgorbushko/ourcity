//
//  UIImage+Base64.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Base64)

+ (instancetype)imageFromBase64String:(NSString *)base64String;

@end
