//
//  UIImage+Base64.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "UIImage+Base64.h"

@implementation UIImage (Base64)

+ (instancetype)imageFromBase64String:(NSString *)base64String
{
    if ([base64String isKindOfClass:NSNull.class] || !base64String) {
        return nil;
    }
    
    NSData *data = [[NSData alloc] initWithBase64EncodedString:base64String options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

@end
