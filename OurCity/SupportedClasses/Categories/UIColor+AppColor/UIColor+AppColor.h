//
//  UIColor+camColor.h
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

@interface UIColor (AppColor)

+ (UIColor *)colorFromHEXString:(NSString *)hexColor;

#pragma mark - TopBarViewColors

+ (UIColor *)lightGrayTopBarColor;

#pragma mark - RegisterViewController

+ (UIColor *)greenBorderColor;

@end
