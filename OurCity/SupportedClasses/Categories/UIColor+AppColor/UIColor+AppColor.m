//
//  UIColor+camColor.m
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

#import "UIColor+AppColor.h"

@implementation UIColor (AppColor)

+ (UIColor *)colorFromHEXString:(NSString *)hexColor
{
    unsigned int hex = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexColor];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hex];
    
    float red = ((float)((hex & 0xFF0000) >> 16))/255;
    float green = ((float)((hex & 0x00FF00) >> 8))/255;
    float blue = ((float)((hex & 0xFF0000) >> 16))/255;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.f];
}

#pragma mark - TopBarViewColors

+ (UIColor *)lightGrayTopBarColor
{
    return [UIColor colorWithRealRed:80 green:80 blue:80 alpha:1.];
}

#pragma mark - RegisterViewController

+ (UIColor *)greenBorderColor
{
    return [UIColor colorWithRealRed:52 green:110 blue:112 alpha:1.];
}

#pragma mark - Private

+ (UIColor *)colorWithRealRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(float)alpha
{
    return [UIColor colorWithRed:((float)red)/255.f green:((float)green)/255.f blue:((float)blue)/255.f alpha:((float)alpha)/1.f];
}

@end
