//
//  Update.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Update : NSObject

@property (assign, nonatomic) NSInteger updateID;
@property (copy, nonatomic) NSString *updateContent;
@property (copy, nonatomic) NSString *updateEducationInstitutionID;
@property (copy, nonatomic) NSString *updateFromDate;
@property (copy, nonatomic) NSString *updateInterestAreaID;
@property (copy, nonatomic) NSString *updateTitle;
@property (copy, nonatomic) NSString *updateToDate;

+ (Update *)getUpdateFromDictionary:(NSDictionary *)updateDictionary;
+ (NSArray *)getAllUpdatingsFromArray:(NSArray *)updatingsArray;

@end
