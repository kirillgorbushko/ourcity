//
//  Update.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "Update.h"

@implementation Update

+ (Update *)getUpdateFromDictionary:(NSDictionary *)updateDictionary
{
    //TODO: parse and convert date ??
    
    Update *update = [Update new];
    update.updateID = [updateDictionary[@"UpdatingId"] integerValue];
    update.updateContent = updateDictionary[@"content"];
    update.updateEducationInstitutionID = updateDictionary[@"educationInstitutionId"];
    update.updateFromDate = updateDictionary[@"fromDate"];
    update.updateInterestAreaID = updateDictionary[@"interestAreaId"];
    update.updateTitle = updateDictionary[@"title"];
    update.updateToDate = updateDictionary[@"toDate"];
    return update;
}

+ (NSArray *)getAllUpdatingsFromArray:(NSArray *)updatingsArray
{
    NSMutableArray *updatings = [NSMutableArray new];
    
    if ([updatingsArray isKindOfClass:NSNull.class]) {
        return updatings;
    }
    
    for (NSDictionary *updateDictionary in updatingsArray) {
        Update *update = [Update getUpdateFromDictionary:updateDictionary];
        [updatings addObject:update];
    }
    
    return updatings;
}

@end
