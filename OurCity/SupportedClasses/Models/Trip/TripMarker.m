//
//  TripMarker.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/31/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "TripMarker.h"

@implementation TripMarker

+ (instancetype)getTripMarkerFromDictionary:(NSDictionary *)markerDictionary
{
    TripMarker *marker = [TripMarker new];
    marker.tripMarkerCityId = [markerDictionary[@"cityId"] integerValue];
    marker.tripMarkerLastModifyUserId = [markerDictionary[@"lastModifyUserId"] integerValue];
    marker.tripMarkerLatitude = [markerDictionary[@"lat"] floatValue];
    marker.tripMarkerLangInfo = markerDictionary[@"latLangInfo"];
    marker.tripMarkerLngId = [markerDictionary[@"latLngId"] integerValue];
    marker.tripMarkerLngName = markerDictionary[@"latLngName"];
    marker.tripMarkerLngType = [markerDictionary[@"latLngType"] integerValue];
    marker.tripMarkerLongitude = [markerDictionary[@"lng"] floatValue];
    marker.tripMarkerTripId = [markerDictionary[@"tripId"] integerValue];
    return marker;
}

+ (NSArray *)getAllMarkersFromArray:(NSArray *)markersArray
{
    NSMutableArray *markers = [NSMutableArray new];
    
    if ([markers isKindOfClass:NSNull.class]) {
        return markers;
    }
    
    for (NSDictionary *markerDictionary in markersArray) {
        TripMarker *marker = [TripMarker getTripMarkerFromDictionary:markerDictionary];
        [markers addObject:marker];
    }
    
    return markers;
}

@end
