//
//  Trip.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/31/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "Trip.h"
#import "TripMarker.h"

@implementation Trip

+ (instancetype)getTripFromDictionary:(NSDictionary *)tripDictionary
{
    Trip *trip = [Trip new];
    trip.tripCityId = [tripDictionary[@"cityId"] integerValue];
    trip.tripDescription = tripDictionary[@"description"];
    trip.tripLastModifyUserId = [tripDictionary[@"lastModifyUserId"] integerValue];
    trip.tripLatitudeLongitudeList = [TripMarker getAllMarkersFromArray:tripDictionary[@"latLngList"]];
    trip.tripId = [tripDictionary[@"tripId"] integerValue];
    trip.tripName = tripDictionary[@"tripName"];
    return trip;
}

+ (NSArray *)getAllTripsFromArray:(NSArray *)tripsArray
{
    NSMutableArray *trips = [NSMutableArray new];
    
    if ([tripsArray isKindOfClass:NSNull.class]) {
        return trips;
    }
    
    for (NSDictionary *tripDictionary in tripsArray) {
        Trip *trip = [Trip getTripFromDictionary:tripDictionary];
        [trips addObject:trip];
    }
    
    return trips;
}

@end
