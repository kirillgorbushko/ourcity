//
//  Trip.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/31/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trip : NSObject

@property (assign, nonatomic) NSInteger tripCityId;
@property (copy, nonatomic) NSString *tripDescription;
@property (assign, nonatomic) NSInteger tripLastModifyUserId;
@property (strong, nonatomic) NSArray *tripLatitudeLongitudeList;
@property (assign, nonatomic) NSInteger tripId;
@property (copy, nonatomic) NSString *tripName;

+ (instancetype)getTripFromDictionary:(NSDictionary *)tripDictionary;
+ (NSArray *)getAllTripsFromArray:(NSArray *)tripsArray;

@end
