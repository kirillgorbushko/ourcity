//
//  TripMarker.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/31/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface TripMarker : NSObject

@property (assign, nonatomic) NSInteger tripMarkerCityId;
@property (assign, nonatomic) NSInteger tripMarkerLastModifyUserId;
@property (assign, nonatomic) CLLocationDegrees tripMarkerLatitude;
@property (copy, nonatomic) NSString *tripMarkerLangInfo;
@property (assign, nonatomic) NSInteger tripMarkerLngId;
@property (copy, nonatomic) NSString *tripMarkerLngName;
@property (assign, nonatomic) NSInteger tripMarkerLngType;
@property (assign, nonatomic) CLLocationDegrees tripMarkerLongitude;
@property (assign, nonatomic) NSInteger tripMarkerTripId;

+ (instancetype)getTripMarkerFromDictionary:(NSDictionary *)markerDictionary;
+ (NSArray *)getAllMarkersFromArray:(NSArray *)markersArray;

@end
