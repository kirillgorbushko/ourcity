//
//  Course.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Course : NSObject

@property (copy, nonatomic) NSString *courseAddress;
@property (assign, nonatomic) NSInteger courseCityID;
@property (strong, nonatomic) NSDate *courseDate;
@property (assign, nonatomic) NSInteger courseID;
@property (copy, nonatomic) NSString *courseDescription;
@property (copy, nonatomic) NSString *courseLink;
@property (copy, nonatomic) NSString *courseNotes;
@property (assign, nonatomic) NSInteger coursePrice;
@property (copy, nonatomic) NSString *courseTitle;

+ (instancetype)getCourseFromDictionary:(NSDictionary *)courseDictionary;
+ (NSArray *)getAllCoursesFromArray:(NSArray *)coursesArray;

@end
