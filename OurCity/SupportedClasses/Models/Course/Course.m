//
//  Course.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "Course.h"

@implementation Course

+ (instancetype)getCourseFromDictionary:(NSDictionary *)courseDictionary
{
    Course *course = [Course new];
    course.courseAddress = courseDictionary[@"address"];
    course.courseCityID = [courseDictionary[@"cityId"] integerValue];
    course.courseDate = [OUCAppHelper dateFromString:courseDictionary[@"coursDate"]];
    course.courseID = [courseDictionary[@"courseId"] integerValue];
    course.courseDescription = courseDictionary[@"description"];
    course.courseLink = courseDictionary[@"link"];
    course.courseNotes = courseDictionary[@"notes"];
    course.coursePrice = [courseDictionary[@"price"] integerValue];
    course.courseTitle = courseDictionary[@"title"];
    return course;
}

+ (NSArray *)getAllCoursesFromArray:(NSArray *)coursesArray
{
    NSMutableArray *courses = [NSMutableArray new];
    
    if ([coursesArray isKindOfClass:NSNull.class]) {
        return courses;
    }
    
    for (NSDictionary *courseDictionary in coursesArray) {
        Course *course = [Course getCourseFromDictionary:courseDictionary];
        [courses addObject:course];
    }
    
    return courses;
}

@end
