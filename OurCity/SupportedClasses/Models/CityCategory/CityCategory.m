//
//  CityCategory.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/29/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "CityCategory.h"
#import "InterestPoint.h"

@implementation CityCategory

+ (instancetype)getCityCategoryFromDictionary:(NSDictionary *)response
{
    CityCategory *category = [CityCategory new];
    category.cityCategoryID = [response[@"CategoryId"] integerValue];
    category.cityCategoryName = response[@"categoryName"];
    category.cityCategoryIcon = response[@"icon"];
    category.cityCategoryInterestPoint = [InterestPoint getAllInterestPointFromArray: response[@"interestPoint"]];
    return category;
}

+ (NSArray *)getAllCityCategoriesFromArray:(NSArray *)response
{
    NSMutableArray *allCategories = [NSMutableArray new];
    
    if ([response isKindOfClass:NSNull.class]) {
        return allCategories;
    }
    
    for (NSDictionary *category in response) {
        CityCategory *newCategory = [CityCategory getCityCategoryFromDictionary:category];
        [allCategories addObject:newCategory];
    }
    
    return allCategories;
}

@end
