//
//  CityCategory.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/29/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityCategory : NSObject

@property (assign, nonatomic) NSInteger cityCategoryID;
@property (copy, nonatomic) NSString *cityCategoryName;
@property (copy, nonatomic) NSString *cityCategoryIcon;
@property (strong, nonatomic) NSArray *cityCategoryInterestPoint;

+ (instancetype)getCityCategoryFromDictionary:(NSDictionary *)response;
+ (NSArray *)getAllCityCategoriesFromArray:(NSArray *)response;

@end
