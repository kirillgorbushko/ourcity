//
//  Message.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "Message.h"

@implementation Message

+ (instancetype)getMessageFromDictionary:(NSDictionary *)messageDictionary
{
    Message *message = [Message new];
    message.messageCityID = [messageDictionary[@"cityId"] integerValue];
    message.messageLastModifyUserID = [messageDictionary[@"lastModifyUserId"] integerValue];
    message.message = messageDictionary[@"message"];
    message.messageToResidentID = [messageDictionary[@"messageToResidentId"] integerValue];
    message.messageResidentID = [messageDictionary[@"residentId"] integerValue];
    return message;
}

+ (NSArray *)getAllMessagesFromArray:(NSArray *)messagesArray
{
    NSMutableArray *messages = [NSMutableArray new];
    
    if ([messagesArray isKindOfClass:NSNull.class]) {
        return messages;
    }
    
    for (NSDictionary *messageDictionary in messagesArray) {
        Message *message = [Message getMessageFromDictionary:messageDictionary];
        [messages addObject:message];
    }
    
    return messages;
}

@end
