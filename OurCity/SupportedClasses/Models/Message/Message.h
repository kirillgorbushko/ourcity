//
//  Message.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (assign, nonatomic) NSInteger messageCityID;
@property (assign, nonatomic) NSInteger messageLastModifyUserID;
@property (copy, nonatomic) NSString *message;
@property (assign, nonatomic) NSInteger messageToResidentID;
@property (assign, nonatomic) NSInteger messageResidentID;

+ (instancetype)getMessageFromDictionary:(NSDictionary *)messageDictionary;
+ (NSArray *)getAllMessagesFromArray:(NSArray *)messagesArray;

@end
