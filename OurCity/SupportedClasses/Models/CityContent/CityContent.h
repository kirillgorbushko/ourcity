//
//  CityContent.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/31/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface CityContent : NSObject

@property (copy, nonatomic) NSString *cityContentAboutCity;
@property (copy, nonatomic) NSString *cityContentBannerImages;
@property (assign, nonatomic) NSInteger cityContentCallCenter;
@property (assign, nonatomic) NSInteger cityContentContentId;
@property (copy, nonatomic) NSString *cityContentHallWebsite;
@property (strong, nonatomic) UIImage *cityContentImage;
@property (assign, nonatomic) CLLocationDegrees cityContentLatitude;
@property (assign, nonatomic) CLLocationDegrees cityContentLongitude;
@property (strong, nonatomic) NSDate *cityContentEmergencyTime;
@property (strong, nonatomic) UIImage *cityContentMayorImage;
@property (copy, nonatomic) NSString *cityContentMayorSpeech;
@property (copy, nonatomic) NSString *cityContentPasswordToCmrcDB;
@property (strong, nonatomic) UIImage *cityContentPromotionalImage;

+ (instancetype)getCityContentFromDictionary:(NSDictionary *)cityContentDictionary;

@end
