//
//  CityContent.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/31/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "CityContent.h"
#import "UIImage+Base64.h"

@implementation CityContent

+ (instancetype)getCityContentFromDictionary:(NSDictionary *)cityContentDictionary
{
    
    CityContent *content = [CityContent new];
    content.cityContentAboutCity = cityContentDictionary[@"aboutCity"];
    content.cityContentBannerImages = cityContentDictionary[@"bannerImages"];
    content.cityContentCallCenter = [cityContentDictionary[@"callCenter"] integerValue];
    content.cityContentContentId = [cityContentDictionary[@"cityContentId"] integerValue];
    content.cityContentHallWebsite = cityContentDictionary[@"cityHallWebsite"];
    content.cityContentImage = [UIImage imageFromBase64String:cityContentDictionary[@"cityImage"]];
    content.cityContentLatitude = [cityContentDictionary[@"cityLat"] floatValue];
    content.cityContentLongitude = [cityContentDictionary[@"cityLng"] floatValue];
    content.cityContentEmergencyTime = [OUCAppHelper dateFromString:cityContentDictionary[@"emergencyTime"]];
    content.cityContentMayorImage = [UIImage imageFromBase64String:cityContentDictionary[@"mayorImage"]];
    content.cityContentMayorSpeech = cityContentDictionary[@"mayorSpeech"];
    content.cityContentPasswordToCmrcDB = cityContentDictionary[@"passwordToCrmcDB"];
    content.cityContentPromotionalImage = [UIImage imageFromBase64String:cityContentDictionary[@"promotionalImage"]];
    return content;
}

@end
