//
//  PhonebookCategory.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "PhonebookCategory.h"

@implementation PhonebookCategory

+ (instancetype)getPhonebookCategoryFromDictionary:(NSDictionary *)phonebookCategoryDictionary
{
    PhonebookCategory *category = [PhonebookCategory new];
    category.phonebookCategoryName = phonebookCategoryDictionary[@"categoryName"];
    category.phonebookCategoryCityID = [phonebookCategoryDictionary[@"cityId"] integerValue];
    category.phonebookCategoryLastModifyUserID = [phonebookCategoryDictionary[@"lastModifyUserId"] integerValue];
    category.phonebookCategoryID = [phonebookCategoryDictionary[@"phonebookCategoryId"] integerValue];
    return category;
}

+ (NSArray *)getAllPhonebookCategoriesFromArray:(NSArray *)phonebookCategoriesArray
{
    NSMutableArray *categories = [NSMutableArray new];
    
    if ([phonebookCategoriesArray isKindOfClass:NSNull.class]) {
        return categories;
    }
    
    for (NSDictionary *categoryDictionary in phonebookCategoriesArray) {
        PhonebookCategory *category = [PhonebookCategory getPhonebookCategoryFromDictionary:categoryDictionary];
        [categories addObject:category];
    }
    
    return categories;
}

@end
