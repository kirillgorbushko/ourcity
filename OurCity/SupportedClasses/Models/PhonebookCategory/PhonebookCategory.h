//
//  PhonebookCategory.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhonebookCategory : NSObject

@property (copy, nonatomic) NSString *phonebookCategoryName;
@property (assign, nonatomic) NSInteger phonebookCategoryCityID;
@property (assign, nonatomic) NSInteger phonebookCategoryLastModifyUserID;
@property (assign, nonatomic) NSInteger phonebookCategoryID;

+ (instancetype)getPhonebookCategoryFromDictionary:(NSDictionary *)phonebookCategoryDictionary;
+ (NSArray *)getAllPhonebookCategoriesFromArray:(NSArray *)phonebookCategoriesArray;

@end
