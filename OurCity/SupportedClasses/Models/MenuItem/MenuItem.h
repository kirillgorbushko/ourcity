//
//  MenuItem.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/28/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

typedef NS_ENUM(NSInteger, ActionType) {
    ActionTypeNone,
    ActionTypeList,
    ActionTypeSubMenu,
    ActionTypeLink,
    ActionTypeDetails,
    ActionTypeMap,
    ActionTypeAppeals,
    ActionTypePolls,
    ActionTypeCall,
    ActionTypeEmail
};

typedef NS_ENUM(NSInteger, ListType) {
    ListTypeNone,
    ListTypeEventsCategory,
    ListTypeAppealsList,
    ListTypePhoneList
};

typedef NS_ENUM(NSInteger, ReturnModelType) {
    ReturnModelTypeNone,
    ReturnModelTypeInt,
    ReturnModelTypeBool,
    ReturnModelTypeString,
    ReturnModelTypeEventsList,
    ReturnModelTypePhoneNumbersList,
    ReturnModelTypeSurveysList
};

@interface MenuItem : NSObject

@property (assign, nonatomic) ActionType menuItemActionType;
@property (strong, nonatomic) UIColor *menuItemColor;
@property (copy, nonatomic) NSString *menuItemLink;
@property (assign, nonatomic) NSInteger menuItemListType;
@property (assign, nonatomic) CLLocationDegrees menuItemMapLatitude;
@property (assign, nonatomic) CLLocationDegrees menuItemMapLongitude;
@property (strong, nonatomic) NSArray *menuItemNodes;
@property (copy, nonatomic) NSString *menuItemPicturePath;
@property (copy, nonatomic) NSString *menuItemRequestJson;
@property (copy, nonatomic) NSString *menuItemRequestRoute;
@property (assign, nonatomic) ReturnModelType menuItemReturnType;
@property (strong, nonatomic) UIImage *menuItemSplashScreen;
@property (copy, nonatomic) NSString *menuItemTitle;

+ (instancetype)getMenuItemFromDictionary:(NSDictionary *)response;
+ (NSArray *)getListOfMenuItemsFromDictionary:(NSDictionary *)response;

@end
