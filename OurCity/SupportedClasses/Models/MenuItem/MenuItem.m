//
//  MenuItem.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/28/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "MenuItem.h"
#import "UIImage+Base64.h"

@implementation MenuItem

+ (instancetype)getMenuItemFromDictionary:(NSDictionary *)response
{
    MenuItem *menuItem = [MenuItem new];
    menuItem.menuItemActionType = [response[@"ActionType"] integerValue];
    menuItem.menuItemColor = [UIColor colorFromHEXString:response[@"Color"]];
    menuItem.menuItemLink = response[@"menuItemLink"];
    menuItem.menuItemListType = [response[@"menuItemListType"] integerValue];
    menuItem.menuItemMapLatitude = [response[@"MapLatitude"] floatValue];
    menuItem.menuItemMapLongitude = [response[@"MapLongitude"] floatValue];
    menuItem.menuItemNodes = [MenuItem getListOfMenuItemsFromDictionary:response[@"Nodes"]];
    menuItem.menuItemPicturePath = response[@"Picture"];
    menuItem.menuItemRequestJson = response[@"RequestJson"];
    menuItem.menuItemRequestRoute = response[@"RequestRoute"];
    menuItem.menuItemReturnType = [response[@"ReturnType"] integerValue];
    menuItem.menuItemSplashScreen = [UIImage imageFromBase64String:response[@"SplashScreen"]];
    menuItem.menuItemTitle = response[@"Title"];
    return menuItem;
}

+ (NSArray *)getListOfMenuItemsFromDictionary:(NSDictionary *)response
{
    NSMutableArray *result = [NSMutableArray new];
    
    if ([response isKindOfClass:NSNull.class]) {
        return result;
    }
    
    for (NSDictionary *item in response) {
        MenuItem *newItem = [MenuItem getMenuItemFromDictionary:item];
        [result addObject:newItem];
    }
    
    return result;
}

@end
