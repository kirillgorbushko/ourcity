//
//  InterestArea.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "InterestArea.h"

@implementation InterestArea

+ (instancetype)getInterestAreaFromDictionary:(NSDictionary *)interestAreaDictionary
{
    InterestArea *area = [InterestArea new];
    area.interestAreaCityID = [interestAreaDictionary[@"cityId"] integerValue];
    area.interestAreaAreaID = [interestAreaDictionary[@"interestAreaId"] integerValue];
    area.interestAreaName = interestAreaDictionary[@"interestAreaName"];
    area.interestAreaToResidentID = [interestAreaDictionary[@"interestAreaToResidentId"] integerValue];
    area.interestAreaLastModifyUserID = [interestAreaDictionary[@"lastModifyUserId"] integerValue];
    return area;
}

+ (NSArray *)getAllInterestAreasFromArray:(NSArray *)interestAreasArray
{
    NSMutableArray *areas = [NSMutableArray new];
    
    if ([interestAreasArray isKindOfClass:NSNull.class]) {
        return areas;
    }
    
    for (NSDictionary *areaDictionary in interestAreasArray) {
        InterestArea *area = [InterestArea getInterestAreaFromDictionary:areaDictionary];
        [areas addObject:area];
    }
    
    return areas;
}

@end
