//
//  InterestArea.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InterestArea : NSObject

@property (assign, nonatomic) NSInteger interestAreaCityID;
@property (assign, nonatomic) NSInteger interestAreaAreaID;
@property (copy, nonatomic) NSString *interestAreaName;
@property (assign, nonatomic) NSInteger interestAreaToResidentID;
@property (assign, nonatomic) NSInteger interestAreaLastModifyUserID;

+ (instancetype)getInterestAreaFromDictionary:(NSDictionary *)interestAreaDictionary;
+ (NSArray *)getAllInterestAreasFromArray:(NSArray *)interestAreasArray;

@end
