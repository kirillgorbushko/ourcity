//
//  Event.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject

@property (assign, nonatomic) NSInteger eventCityID;
@property (assign, nonatomic) NSInteger eventLastModityUserID;
@property (copy, nonatomic) NSString *eventAddress;
@property (copy, nonatomic) NSString *eventDescription;
@property (strong, nonatomic) NSDate *eventDate;
@property (strong, nonatomic) NSDate *eventDateToMobileClient;
@property (assign, nonatomic) NSInteger eventID;
@property (copy, nonatomic) NSString *eventLink;
@property (copy, nonatomic) NSString *eventNotes;
@property (copy, nonatomic) NSString *eventPicture;
@property (copy, nonatomic) NSString *eventPrice;
@property (copy, nonatomic) NSString *eventTitle;
@property (assign, nonatomic) NSInteger eventTypeID;
@property (copy, nonatomic) NSString *eventTypeName;

+ (instancetype)getEventFromDictionary:(NSDictionary *)eventDictionary;
+ (NSArray *)getAllEventsFromArray:(NSArray *)events;

@end
