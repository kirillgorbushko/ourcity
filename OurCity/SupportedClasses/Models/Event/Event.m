//
//  Event.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "Event.h"

@implementation Event

+ (instancetype)getEventFromDictionary:(NSDictionary *)eventDictionary
{
    Event *event = [Event new];
    event.eventCityID = [eventDictionary[@"cityId"] integerValue];
    event.eventLastModityUserID = [eventDictionary[@"lastModifyUserId"] integerValue];
    event.eventAddress = eventDictionary[@"address"];
    event.eventDescription = eventDictionary[@"description"];
    event.eventDate = [OUCAppHelper dateFromString:eventDictionary[@"eventDate"]];
    event.eventDateToMobileClient = [OUCAppHelper dateFromString:eventDictionary[@"eventDateToMobileClient"]];
    event.eventID = [eventDictionary[@"eventId"] integerValue];
    event.eventLink = eventDictionary[@"link"];
    event.eventNotes = eventDictionary[@"notes"];
    event.eventPicture = eventDictionary[@"picture"];
    event.eventPrice = eventDictionary[@"price"];
    event.eventTitle = eventDictionary[@"title"];
    event.eventTypeID = [eventDictionary[@"typeId"] integerValue];
    event.eventTypeName = eventDictionary[@"typeName"];
    return event;
}

+ (NSArray *)getAllEventsFromArray:(NSArray *)events
{
    NSMutableArray *eventsList = [NSMutableArray new];
    
    if ([events isKindOfClass:NSNull.class]) {
        return eventsList;
    }
    
    for (NSDictionary *event in events) {
        Event *newEvent = [Event getEventFromDictionary:event];
        [eventsList addObject:newEvent];
    }
    
    return eventsList;
}

@end
