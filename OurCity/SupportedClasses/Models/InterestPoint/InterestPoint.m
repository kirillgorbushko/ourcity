//
//  InterestPoint.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "InterestPoint.h"

@implementation InterestPoint

+ (instancetype)getInterestPointFromDictionary:(NSDictionary *)dictionary
{
    InterestPoint *point = [InterestPoint new];
    point.interestPointCategoryId = [dictionary[@"categoryId"] integerValue];
    point.interestPointCityId = [dictionary[@"cityId"] integerValue];
    point.interestPointDescription = dictionary[@"description"];
    point.interestPointId = [dictionary[@"interestPointId"] integerValue];
    point.interestPointLatitude = [dictionary[@"lat"] floatValue];
    point.interestPointLongitude = [dictionary[@"lon"] floatValue];
    return point;
}

+ (NSArray *)getAllInterestPointFromArray:(NSArray *)array;
{
    NSMutableArray *points = [NSMutableArray new];
    
    if ([array isKindOfClass:NSNull.class]) {
        return points;
    }
    
    for (NSDictionary *point in array) {
        InterestPoint *newPoint = [InterestPoint getInterestPointFromDictionary:point];
        [points addObject:newPoint];
    }
    
    return points;
}

@end
