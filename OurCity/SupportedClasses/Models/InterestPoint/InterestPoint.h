//
//  InterestPoint.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface InterestPoint : NSObject

@property (assign, nonatomic) NSInteger interestPointCategoryId;
@property (assign, nonatomic) NSInteger interestPointCityId;
@property (copy, nonatomic) NSString *interestPointDescription;
@property (assign, nonatomic) NSInteger interestPointId;
@property (assign, nonatomic) CLLocationDegrees interestPointLatitude;
@property (assign, nonatomic) CLLocationDegrees interestPointLongitude;

+ (instancetype)getInterestPointFromDictionary:(NSDictionary *)dictionary;
+ (NSArray *)getAllInterestPointFromArray:(NSArray *)dictionary;

@end
