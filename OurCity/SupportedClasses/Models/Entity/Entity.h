//
//  Entity.h
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Entity : NSObject

@property (assign, nonatomic) NSInteger entityCityID;
@property (copy, nonatomic) NSString *entityDetails;
@property (copy, nonatomic) NSString *entityAddress;
@property (assign, nonatomic) NSInteger entityCategoryID;
@property (assign, nonatomic) NSInteger entityID;
@property (copy, nonatomic) NSString *entityName;
@property (copy, nonatomic) NSString *entityInformation;
@property (assign, nonatomic) NSInteger entityLastModifyUserID;
@property (assign, nonatomic) NSInteger entityPhoneNumber;

+ (instancetype)getEntityFromDictionary:(NSDictionary *)entityDictionary;
+ (NSArray *)getAllEntitiesFromArray:(NSArray *)entitiesArray;

@end
