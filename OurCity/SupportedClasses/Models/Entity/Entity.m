//
//  Entity.m
//  OurCity
//
//  Created by Yuriy Sirko on 7/30/15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "Entity.h"

@implementation Entity

+ (instancetype)getEntityFromDictionary:(NSDictionary *)entityDictionary
{
    Entity *entity = [Entity new];
    entity.entityCityID = [entityDictionary[@"cityId"] integerValue];
    entity.entityDetails = entityDictionary[@"details"];
    entity.entityAddress = entityDictionary[@"emailAddress"];
    entity.entityCategoryID = [entityDictionary[@"entityCategoryId"] integerValue];
    entity.entityID = [entityDictionary[@"entityId"] integerValue];
    entity.entityName = entityDictionary[@"entityName"];
    entity.entityInformation = entityDictionary[@"information"];
    entity.entityLastModifyUserID = [entityDictionary[@"lastModifyUserId"] integerValue];
    entity.entityPhoneNumber = [entityDictionary[@"phoneNumber"] integerValue];
    return entity;
}

+ (NSArray *)getAllEntitiesFromArray:(NSArray *)entitiesArray
{
    NSMutableArray *entities = [NSMutableArray new];
    
    if ([entitiesArray isKindOfClass:NSNull.class]) {
        return entities;
    }
    
    for (NSDictionary *entityDictionary in entitiesArray) {
        Entity *entity = [Entity getEntityFromDictionary:entityDictionary];
        [entities addObject:entity];
    }
    
    return entities;
}

@end
