# README #

OurCity

### What is this repository for? ###

* Developing OurCity

### How do I get set up? ###

* Clone repo in to your dir

### Contribution guidelines ###

* Write code on new branch from develop name like - _issue_ISSUE_NAME
* Push new branch for review
* Set issue on tracker to status OnHold
* Wait review
* Merge your branch with develop

### Who do I talk to? ###

* Repo owner or admin

### Additional Information ###

https://drive.google.com/folderview?id=0B1GU18BxUf8hfjdiZTNQSEpvYVVETDluU2Z6eXZzSlpIWF9ldDNuTWdPX3hzb3dZRlg2Q0k&usp=sharing

All project should use Xibs instead of storyboards